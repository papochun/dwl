/* appearance */
static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 4;  /* border pixel of windows */
static const int lockfullscreen     = 1;  /* 1 will force focus on the fullscreen window */
static const float rootcolor[]      = {1.0, 1.0, 0.0, 1.0};
static const float bordercolor[]    = {0.5, 0.5, 0.5, 1.0};
static const float focuscolor[]     = {1.0, 0.0, 0.0, 1.0};

/* tagging */
static const char *tags[] = { "", "", "", "", };

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	*/
	{ "firefox",  NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "T",      tile },
	{ "F",      NULL },    /* no layout function means floating behavior */
	{ "M",      monocle },
};

/* monitors */
static const MonitorRule monrules[] = {
        /* name       mfact nmaster scale layout       rotate/reflect x y resx resy rate adaptive*/
        /* example of a HiDPI laptop monitor at 120Hz:
        { "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 120.000, 1},
	*/
	/* defaults */
        { NULL,       0.50, 1, 1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 0, 0},

        { "HDMI-A-1", 0.50, 1, 1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 144, 0},
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
	.options = "",
};

static const int repeat_rate = 50;
static const int repeat_delay = 250;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;
/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;
/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;
/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;

/* If you want to use the windows key change this to WLR_MODIFIER_LOGO */
#define MOD WLR_MODIFIER_LOGO
#define ALT WLR_MODIFIER_ALT
#define CTRL WLR_MODIFIER_CTRL
#define SHIFT WLR_MODIFIER_SHIFT
#define TAGKEYS(KEY,SKEY,TAG) \
	{ MOD,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MOD|CTRL,               KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MOD|SHIFT,              SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MOD|CTRL|SHIFT,         SKEY,toggletag, {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#define EXEC(...) { .v = (const char*[]) {__VA_ARGS__, NULL }}

static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
        { MOD|SHIFT,                    XKB_KEY_Right,       spawn,          EXEC("cmus-remote", "-n") },
        { MOD|SHIFT,                    XKB_KEY_Left,        spawn,          EXEC("cmus-remote", "-r") },
        { MOD,                          XKB_KEY_Right,       spawn,          EXEC("cmus-remote", "-k", "+5") },
        { MOD,                          XKB_KEY_Left,        spawn,          EXEC("cmus-remote", "-k", "-5") },
        { MOD,                          XKB_KEY_Up,          spawn,          EXEC("cmus-remote", "-v", "+5%") },
        { MOD,                          XKB_KEY_Down,        spawn,          EXEC("cmus-remote", "-v", "-5%") },
        { MOD,                          XKB_KEY_apostrophe,  spawn,          EXEC("cmus-remote", "-u") },

        { MOD,                          XKB_KEY_z,           spawn,          EXEC("bluetoothctl", "power", "on") },
        { MOD|SHIFT,                    XKB_KEY_Z,           spawn,          EXEC("bluetoothctl", "power", "off") },
/*
        { MOD,                          XKB_KEY_f,           spawn,          EXEC("feed.sh") },
        { MOD,                          XKB_KEY_s,           spawn,          EXEC("setting.sh") },
        { MOD,                          XKB_KEY_v,           spawn,          EXEC("view.sh") },
        { MOD,                          XKB_KEY_g,           spawn,          EXEC("game.sh") },
        { MOD,                          XKB_KEY_w,           spawn,          EXEC("2fa.sh") },
        { MOD,                          XKB_KEY_p,           spawn,          EXEC("spectre.sh") },
        { MOD,                          XKB_KEY_r,           spawn,          EXEC("refresh.sh") },
*/

        { MOD,                          XKB_KEY_d,           spawn,          EXEC("bemenu-run") },
        { MOD,                          XKB_KEY_Return,      spawn,          EXEC("alacritty") },



	{ MOD,                    XKB_KEY_j,          focusstack,     {.i = +1} },
	{ MOD,                    XKB_KEY_k,          focusstack,     {.i = -1} },
	{ MOD,                    XKB_KEY_h,          setmfact,       {.f = -0.05} },
	{ MOD,                    XKB_KEY_l,          setmfact,       {.f = +0.05} },
	{ MOD,                    XKB_KEY_Return,     zoom,           {0} },
	{ MOD,                    XKB_KEY_Tab,        view,           {0} },
	{ MOD|SHIFT,              XKB_KEY_C,          killclient,     {0} },
	{ MOD,                    XKB_KEY_t,          setlayout,      {.v = &layouts[0]} },
	{ MOD,                    XKB_KEY_f,          setlayout,      {.v = &layouts[1]} },
	{ MOD,                    XKB_KEY_m,          setlayout,      {.v = &layouts[2]} },
	{ MOD,                    XKB_KEY_space,      setlayout,      {0} },
	{ MOD|SHIFT,              XKB_KEY_space,      togglefloating, {0} },
	{ MOD|SHIFT,              XKB_KEY_F,          togglefullscreen, {0} },
	{ MOD|SHIFT,              XKB_KEY_parenright, tag,            {.ui = ~0} },
	{ MOD,                    XKB_KEY_comma,      focusmon,       {.i = WLR_DIRECTION_LEFT} },
	{ MOD,                    XKB_KEY_period,     focusmon,       {.i = WLR_DIRECTION_RIGHT} },
	{ MOD|SHIFT,              XKB_KEY_less,       tagmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MOD|SHIFT,              XKB_KEY_greater,    tagmon,         {.i = WLR_DIRECTION_RIGHT} },

	{ MOD|SHIFT,              XKB_KEY_Q,          quit,           {0} },

	TAGKEYS( XKB_KEY_y, XKB_KEY_Y, 0),
	TAGKEYS( XKB_KEY_u, XKB_KEY_U, 1),
	TAGKEYS( XKB_KEY_i, XKB_KEY_I, 2),
	TAGKEYS( XKB_KEY_o, XKB_KEY_O, 3),

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_Terminate_Server, quit, {0} },
#define CHVT(n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MOD, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MOD, BTN_MIDDLE, togglefloating, {0} },
	{ MOD, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
